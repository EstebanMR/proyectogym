﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ProyectoGYM.BOL;
using ProyectoGYM.Entities;

namespace ProyectoGYM.GUI
{
    public partial class frmAgreEjer : Form
    {
        private EjercicioBOL ejBOL;

        private EEjercicio ejercicio;
        public frmAgreEjer()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                if (dgvEjercicios.SelectedRows.Count > 0)
                {
                    DataGridViewRow fila = dgvEjercicios.SelectedRows[0];
                    string nombre = (String)fila.Cells[2].Value;
                    List<EEjercicio> lista = ejBOL.CargarTabla(nombre);
                    ejercicio = lista[0];
                    Close();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        private void frmAgreEjer_Load(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                dgvEjercicios.DataSource = ejBOL.CargarTabla1();
            }
            catch (Exception ex)
            {
                string sms = ex.Message;
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = sms;
                MessageBox.Show(sms);
            }
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                if (txtFiltro.TextLength >= 3)
                {
                    dgvEjercicios.DataSource = ejBOL.CargarTabla(txtFiltro.Text);
                    dgvEjercicios.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        public Object GetEjercicio()
        {
            return ejercicio;
        }
    }
}
