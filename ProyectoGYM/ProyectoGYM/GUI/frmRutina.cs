﻿using ProyectoGYM.BOL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ProyectoGYM.Entities;

namespace ProyectoGYM.GUI
{
    public partial class frmRutina : Form
    {
        RutinaBOL RLog;
        public frmRutina()
        {
            InitializeComponent();
        }

        private void frmRutina_Load(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                dgvRutinas.DataSource = RLog.CargarTodos(null);
                dgvRutinas.ClearSelection();
                Limpiar();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        private void Limpiar()
        {
            txtNombre.Text = string.Empty;
            txtNombre.Tag = 0;
            txtDescripcion.Text = string.Empty;
            lEjercicios.Items.Clear();

        }

        private void dgvRutinas_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                CargarMante();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        private void CargarMante()
        {
            try
            {
                lblMensaje.Text = String.Empty;
                if (dgvRutinas.SelectedRows.Count > 0)
                {
                    DataGridViewRow fila = dgvRutinas.SelectedRows[0];
                    string nom = (string)fila.Cells[2].Value;
                    List<ERutina> rutinas = RLog.CargarTodos(nom);
                    ERutina rut = rutinas[0];
                    txtNombre.Text = rut.Nombre;
                    txtNombre.Tag = rut.Id;
                    txtDescripcion.Text = rut.Descripcion;
                    lEjercicios.Items.Add(rut.Ejercicios);
                }
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                Limpiar();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        private void btnEjercicios_Click(object sender, EventArgs e)
        {
            frmAgreEjer frm = new frmAgreEjer();
            frm.ShowDialog();
            EEjercicio temp = (EEjercicio)frm.GetEjercicio();
        }
    }
}
