﻿using ProyectoGYM.BOL;
using ProyectoGYM.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoGYM.GUI
{
    public partial class frmLogin : Form
    {
        internal LogicaBOL logica { get; set; }
        public frmLogin()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmRegistro frm = new frmRegistro();
            Hide();
            frm.ShowDialog();
            Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (logica.user == null)
            {
                logica.user = new Entities.EUsuario();
            }
            try
            {
                logica.user.Cedula = txtCed.Text;
                logica.user.Contrasena = txtContra.Text;
                logica.user = new UsuarioBOL().Iniciar(logica.user);

                if (logica.user.Correo != null)
                {
                    string message = "Ingreso exitoso";
                    string caption = "Login ";
                    MessageBoxButtons buttons = MessageBoxButtons.OK;
                    DialogResult result = MessageBox.Show(message, caption, buttons, MessageBoxIcon.Information);


                    Close();
                }
            }
            catch (Exception)
            {
                string message = "Favor revise los datos ingresados, estos deben ser válidos";
                string caption = "Login inválido";
                MessageBoxButtons buttons = MessageBoxButtons.OKCancel;
                DialogResult result = MessageBox.Show(message, caption, buttons, MessageBoxIcon.Warning);
                if (result == DialogResult.Cancel)
                {
                    Application.Exit();
                }
            }
        }
    }
}
