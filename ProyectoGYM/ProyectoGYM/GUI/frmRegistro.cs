﻿using ProyectoGYM.BOL;
using ProyectoGYM.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoGYM.GUI
{
    public partial class frmRegistro : Form
    {
        UsuarioBOL LogU = new UsuarioBOL();
        public frmRegistro()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                EUsuario Nuevo = new EUsuario
                {
                    Nombre = txtNombre.Text,
                    Cedula = txtCedula.Text,
                    Correo = txtCorreo.Text,
                    FechaNac = dtFechaNac.Value,
                    Contrasena = txtContra.Text,
                };

                LogU.Agregar(Nuevo);

                string message = "Registro exitoso";
                string caption = "Registo";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                DialogResult result = MessageBox.Show(message, caption, buttons, MessageBoxIcon.Information);
                Close();
            }
            catch (Exception ex)
            {
                string message = "Favor revise los datos ingresados, estos deben ser válidos "+ex;
                string caption = "Registo inválido";
                MessageBoxButtons buttons = MessageBoxButtons.OKCancel;
                DialogResult result = MessageBox.Show(message, caption, buttons, MessageBoxIcon.Warning);
            }

        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            Close();
        }
    }
}
