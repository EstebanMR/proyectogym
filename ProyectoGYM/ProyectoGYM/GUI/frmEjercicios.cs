﻿using ProyectoGYM.BOL;
using ProyectoGYM.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ProyectoGYM.GUI
{
    public partial class frmEjercicios : Form
    {
        private EjercicioBOL ejBOL;

        public frmEjercicios()
        {
            InitializeComponent();
            ejBOL = new EjercicioBOL();
        }

        /// <summary>
        /// llama al metodo de limpiar el mantenimiento
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                Limpiar();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        /// <summary>
        /// limpiador del mantenimiento de usuarios
        /// </summary>
        private void Limpiar()
        {
            txtNombre.Text = "";
            txtNombre.Tag = 0;
            txtDescripcion.Text = "";
            nudRep.Value = 1;
            nudSegundos.Value = 1;
        }

        /// <summary>
        /// Llama al metodo de guardar datos del mantenimiento
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                GuardarDatos();
            }
            catch (Exception ex)
            {
                string sms = ex.Message ;
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = sms;
                //MessageBox.Show(sms, "Mesage de error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// toma los datos del mantenimiento y verifica si el usuario es nuevo o ya está ingresado a la base y con ello 
        /// llama a actualizar o a insertar
        /// </summary>
        private void GuardarDatos()
        {
            EEjercicio temp = new EEjercicio
            {               
                Nombre = txtNombre.Text,
                Descripcion = txtDescripcion.Text,
                Repeticiones = (int)nudRep.Value,
                Segundos = (int)nudSegundos.Value
            };
            Console.WriteLine(Convert.ToInt32(txtNombre.Tag));
            if (Convert.ToInt32(txtNombre.Tag) != 0)
            {
                Console.WriteLine(temp.Id);
                temp.Id = (int)txtNombre.Tag;
                ejBOL.Actualizar(temp);
            }
            else
            {
                Console.WriteLine(temp.Nombre);
                ejBOL.Insertar(temp);
            }
        }

        /// <summary>
        /// carga el filtro de la tabla
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                string filtro = txtFiltro.Text;
                if (filtro.Length >= 3)
                {
                    dgvEjercicios.DataSource = ejBOL.CargarTabla(filtro);
                }
            }
            catch (Exception ex)
            {
                lblMensaje.BackColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        private void FrmEjercicios_Load(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                dgvEjercicios.DataSource = ejBOL.CargarTabla(null);

            }
            catch (Exception ex)
            {
                lblMensaje.BackColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        private void DgvEjercicios_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                CargarMante();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        private void CargarMante()
        {
            try
            {
                lblMensaje.Text = String.Empty;
                if (dgvEjercicios.SelectedRows.Count > 0)
                {
                    DataGridViewRow Fila = dgvEjercicios.SelectedRows[0];
                    string temp = (string)Fila.Cells[2].Value;
                    Console.WriteLine(temp);
                    List<EEjercicio> us = ejBOL.CargarTabla(temp);
                    EEjercicio u = us[0];
                    txtNombre.Text = u.Nombre;
                    txtNombre.Tag = u.Id;
                    txtDescripcion.Text = u.Descripcion;
                    nudRep.Value = u.Repeticiones;
                    nudSegundos.Value = u.Segundos;
                }
                dgvEjercicios.ClearSelection();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }
    }
}
