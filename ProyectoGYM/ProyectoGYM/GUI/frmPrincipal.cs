﻿using ProyectoGYM.BOL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoGYM.GUI
{
    public partial class frmPrincipal : Form
    {
        internal LogicaBOL log;
        public frmPrincipal()
        {
            InitializeComponent();
            log = new LogicaBOL();
            ValidarLogin();
        }

        public void ValidarLogin()
        {
            try
            {
                if (log.user == null)
                {
                    frmLogin frm = new frmLogin { logica = log };
                    Hide();
                    frm.ShowDialog();
                    Show();
                    if (log.user.Tipo == 1)
                    {
                        frmAdmin frma = new frmAdmin { Log = log };
                        Hide();
                        frma.ShowDialog();
                        Show();
                    }
                    else if (log.user.Tipo == 2)
                    {
                        frmEntrenador frme = new frmEntrenador { Logi = log };
                        Hide();
                        frme.ShowDialog();
                        Show();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            log.user = null;
            ValidarLogin();
        }

        private void pagarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPago frm = new frmPago();
            Hide();
            frm.ShowDialog();
            Show();
        }
    }
}
