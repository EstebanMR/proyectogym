﻿using ProyectoGYM.BOL;
using ProyectoGYM.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoGYM.GUI
{
    public partial class frmAdmin : Form
    {

        internal LogicaBOL Log;
        internal UsuarioBOL Uslog;
        public frmAdmin()
        {
            InitializeComponent();
            Log = new LogicaBOL();
            Uslog = new UsuarioBOL();
        }

        /// <summary>
        /// Se encarga de cargar la ventana y la tabla con datos
        /// </summary>
        /// <param name="sender"></param> parametro por defecto
        /// <param name="e"></param> parametro ppor defecto
        private void frmAdmin_Load(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                dgvUsuarios.DataSource = Uslog.CargarTodos(null);
                dgvUsuarios.ClearSelection();
                Limpiar();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        /// <summary>
        /// obtiene el texto del filtro para mandar a pedir los datos de la base de datos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                string filtro = txtFiltro.Text.Trim();
                if (filtro.Length >= 3)
                {
                    dgvUsuarios.DataSource = Uslog.CargarTodos(filtro);
                }
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }

        }

        /// <summary>
        /// se encarga de determinar si una fila de la tabla ha sido seleccionada para mandar a cargar los datos en el mantenimiento
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                CargarMante();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }

        }

        /// <summary>
        /// se encarga de obtener el usuario de la fila seleccionada por el usuario para cargar el mantenimiento
        /// </summary>
        private void CargarMante()
        {
            try
            {
                lblMensaje.Text = String.Empty;
                if (dgvUsuarios.SelectedRows.Count > 0)
                {
                    DataGridViewRow Fila = dgvUsuarios.SelectedRows[0];
                    string temp = (string)Fila.Cells[3].Value;
                    //Console.WriteLine(temp);
                    List<EUsuario> us = Uslog.CargarTodos(temp);
                    EUsuario u = us[0];
                    txtNombre.Text = u.Nombre;
                    txtNombre.Tag = u.Id;
                    txtCorreo.Text = u.Correo;
                    txtCedula.Text = u.Cedula;
                    dtpFecha.Value = u.FechaNac;
                    nudTipo.Value = u.Tipo;
                    txtContra.Text = u.Contrasena;
                    chkActivo.Checked = u.Activo;
                }
                dgvUsuarios.ClearSelection();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        /// <summary>
        /// se encarga de llamar al metodo de cargar mantenimiento de usuarios
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvUsuarios_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                CargarMante();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        /// <summary>
        /// se encarga de llamar al limpiardor del mantenimiento de usuarios
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                Limpiar();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        /// <summary>
        /// limpiador del mantenimiento de usuarios
        /// </summary>
        private void Limpiar()
        {
            txtNombre.Text = "";
            txtNombre.Tag = 0;
            txtCorreo.Text = "";
            txtCedula.Text = "";
            dtpFecha.Value = DateTime.Today;
            nudTipo.Value = 3;
            txtContra.Text = "";
            chkActivo.Checked = true;
        }

        /// <summary>
        /// se encarga de llamar al actualizar los datos de usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                GuardarDatos();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        /// <summary>
        /// se encarga de recolectar los datos actualizados del mantenimiento de usuarios
        /// </summary>
        private void GuardarDatos()
        {
            try
            {
                lblMensaje.Text = String.Empty;
                EUsuario u = new EUsuario
                {
                    Nombre = txtNombre.Text,
                    Cedula = txtCedula.Text,
                    Correo = txtCorreo.Text,
                    FechaNac = dtpFecha.Value,
                    Tipo = (int)nudTipo.Value,
                    Contrasena = txtContra.Text,
                    Activo = chkActivo.Checked
                };
                if ((int)txtNombre.Tag != 0)
                {
                    u.Id = (int)txtNombre.Tag;
                    Uslog.ActualizarDatos(u);

                }
                else if ((int)txtNombre.Tag==0)
                {
                    Uslog.Agregar(u);
                }
                else if(u==null)
                {
                    string message = "Favor seleccionar un usuario";
                    string caption = "Administrador ";
                    MessageBoxButtons buttons = MessageBoxButtons.OKCancel;
                    MessageBox.Show(message, caption, buttons, MessageBoxIcon.Information);
                }
                dgvUsuarios.DataSource = Uslog.CargarTodos(null);
                dgvUsuarios.ClearSelection();
                Limpiar();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }

        }

        /// <summary>
        /// se encarga de salir de la aplicación de manera más sencilla
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void DgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
            Log.user = null;
            frmPrincipal frm = new frmPrincipal();
            frm.ValidarLogin();
        }
    }
}