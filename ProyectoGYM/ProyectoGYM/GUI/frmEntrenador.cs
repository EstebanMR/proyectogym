﻿using ProyectoGYM.BOL;
using ProyectoGYM.Entities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ProyectoGYM.GUI
{
    public partial class frmEntrenador : Form
    {
        internal LogicaBOL Logi;
        internal UsuarioBOL Uslog;
        internal EUsuario entrenado { get; set; }
        public frmEntrenador()
        {
            InitializeComponent();
            Logi = new LogicaBOL();
            Uslog = new UsuarioBOL();
        }

        private void msRutinas_Click(object sender, EventArgs e)
        {
            frmRutina frm = new frmRutina();
            Hide();
            frm.ShowDialog();
            Show();
        }

        private void msEjercicios_Click(object sender, EventArgs e)
        {
            frmEjercicios frm = new frmEjercicios();
            Hide();
            frm.ShowDialog();
            Show();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void CerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
            Logi.user = null;
            frmPrincipal frm = new frmPrincipal();
            frm.ValidarLogin();
        }

        private void frmEntrenador_Load(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                dgvUsuarios.DataSource = Uslog.CargarUsuarios();
                dgvUsuarios.ClearSelection();
                Limpiar();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        private void Limpiar()
        {
            entrenado = null;
            lblUsuario.Text = String.Empty;
        }

        private void txtFiltro_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                CargarMante();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        private void CargarMante()
        {
            try
            {
                lblMensaje.Text = String.Empty;
                if (dgvUsuarios.SelectedRows.Count > 0)
                {
                    DataGridViewRow Fila = dgvUsuarios.SelectedRows[0];
                    string temp = (string)Fila.Cells[3].Value;
                    List<EUsuario> us = Uslog.CargarTodos(temp);
                    entrenado = us[0];
                    lblUsuario.Text = entrenado.Nombre;
                    lblUsuario.Tag = entrenado.Id;
                }
                dgvUsuarios.ClearSelection();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                lblMensaje.Text = String.Empty;
                dgvUsuarios.DataSource = Uslog.CargarUsuarios1();
                dgvUsuarios.ClearSelection();
                Limpiar();
            }
            catch (Exception ex)
            {
                lblMensaje.ForeColor = Color.IndianRed;
                lblMensaje.Text = ex.Message;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
