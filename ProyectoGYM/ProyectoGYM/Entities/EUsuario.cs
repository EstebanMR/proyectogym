﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoGYM.Entities
{
    class EUsuario
    {
        public int Id { get; set; }
        public bool Activo { get; set; }
        public string Correo { get; set; }
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public int Tipo { get; set; }
        public DateTime FechaNac { get; set; }
        public string Contrasena { get; set; }
    }
}
