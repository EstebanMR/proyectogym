﻿using ProyectoGYM.DAL;
using ProyectoGYM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoGYM.BOL
{
    class EjercicioBOL
    {
        internal List <EEjercicio> CargarTabla(string filtro)
        {
            if (filtro == null)
            {
                Console.WriteLine(1);
                return new EjercicioDAL().CargarTodos();
            }
            else
            {
                Console.WriteLine(2);
                return new EjercicioDAL().CargarTodosFiltro(filtro);
            }
        }

        internal void Actualizar(EEjercicio temp)
        {
            new EjercicioDAL().Actualizar(temp);
        }

        internal void Insertar(EEjercicio temp)
        {
            new EjercicioDAL().Insertar(temp);
        }

        internal List<EEjercicio> CargarTabla1()
        {
            return new EjercicioDAL().CargarTodos();
        }
    }
}
