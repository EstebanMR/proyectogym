﻿using ProyectoGYM.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectoGYM.DAL;

namespace ProyectoGYM.BOL
{
    class RutinaBOL
    {
        internal List<ERutina> CargarTodos(string filtro)
        {
            if (filtro != null)
            {
                return new RutinaDAL().CargarFiltro(filtro);
            }
            else
            {
                return new RutinaDAL().CargarTodos();
            }
        }
    }
}
