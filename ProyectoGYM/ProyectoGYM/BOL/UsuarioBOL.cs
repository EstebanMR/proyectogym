﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProyectoGYM.DAL;
using ProyectoGYM.Entities;

namespace ProyectoGYM.BOL
{
    class UsuarioBOL
    {
        /// <summary>
        /// Manda los datos del registro a la capa de datos para crear nuevos usuarios
        /// </summary>
        /// <param name="nuevo"></param> los datos del nuevo usuario encapsulados en una entidad de EUsuario
        internal void Agregar(EUsuario nuevo)
        {
            new UsuarioDAL().Registrar(nuevo);
        }

        /// <summary>
        /// manda a traer los datos a la capa de datos con base en la información del login
        /// </summary>
        /// <param name="user"></param> información de usuario proporcionada en login encapsulada en un EUsuario
        /// <returns></returns> retorna todos los datos completos del usuario en un EUsuario
        internal EUsuario Iniciar(EUsuario user)
        {
            return new UsuarioDAL().Inicio(user);
        }

        /// <summary>
        /// Según un filtro que manda a pedir a la base de datos todos los usuarios
        /// </summary>
        /// <param name="filtro"></param> filtro de cedula  para buscar usuarios
        /// <returns></returns> retorna una lista con todos los usuarios según el filtro o no
        internal List<EUsuario> CargarTodos(string filtro)
        {
            
            if (filtro == null)
            {
                return new UsuarioDAL().CargarTodos();
            }
            return new UsuarioDAL().CargarConFiltro(filtro);
        }

        internal List<EUsuario> CargarUsuarios()
        {
            return new UsuarioDAL().CargarUsuarios1();
        }

        internal List<EUsuario> CargarUsuarios1()
        {
            return new UsuarioDAL().CargarUsuarios();
        }

        /// <summary>
        /// manda el EUsuario actualizado para la base de datos
        /// </summary>
        /// <param name="u"></param> datos actualizados del usuario en EUsuario
        internal void ActualizarDatos(EUsuario u)
        {
            new UsuarioDAL().ActualizarDatos(u);
        }
    }
}
