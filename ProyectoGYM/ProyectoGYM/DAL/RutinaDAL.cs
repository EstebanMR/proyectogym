﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using ProyectoGYM.Entities;

namespace ProyectoGYM.DAL
{
    class RutinaDAL
    {
        internal List<ERutina> CargarFiltro(string filtro)
        {
            List<ERutina> lista = new List<ERutina>();
            string sql = "SELECT id, nombre, descripcion, ejercicios" +
                " FROM app.rutinas" +
                " WHERE nombre like '" + filtro + "'";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lista.Add(ArmarRutina(reader));
                }
            }

            return lista;
        }

        internal List<ERutina> CargarTodos()
        {
            List<ERutina> lista = new List<ERutina>();
            string sql = "SELECT id, nombre, descripcion, ejercicios" +
                " FROM app.rutinas";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lista.Add(ArmarRutina(reader));
                }
            }

            return lista;
        }

        private ERutina ArmarRutina(NpgsqlDataReader reader)
        {
            ERutina temp = new ERutina
            {
                Id = reader.GetInt32(0),
                Nombre = reader.GetString(1),
                Descripcion = reader.GetString(2),
                Ej = AE(reader.GetString(3)),
            };
            temp.Ejercicios = AS(temp.Ej);
            return temp;  
        }

        private string AS(List<EEjercicio> ej)
        {
            string temp = "";
            foreach (EEjercicio i in ej)
            {
                temp += i.Nombre + ", ";
            }            
            return temp;
        }

        private List<EEjercicio> AE(string v)
        {
            List<EEjercicio> ejer = new List<EEjercicio>();
            string[] num = v.Split(',');
            foreach (string i in num)
            {
                ejer.Add(new EjercicioDAL().CargarId(Convert.ToInt32(i)));
            }
            return ejer;
        }
    }
}
