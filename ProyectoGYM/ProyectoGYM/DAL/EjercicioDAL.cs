﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using ProyectoGYM.Entities;

namespace ProyectoGYM.DAL
{
    class EjercicioDAL
    {
        internal List<EEjercicio> CargarTodos()
        {
            List<EEjercicio> lista = new List<EEjercicio>();
            string sql = "SELECT id, nombre, descripcion, repeticiones, segundos FROM app.ejercicios";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lista.Add(ArmarEjercicio(reader));
                }
            }

            return lista;
        }

        internal void Insertar(EEjercicio temp)
        {
            string sql = "INSERT INTO app.ejercicios(" +
                " nombre, descripcion, repeticiones, segundos)" +
                " VALUES( @nom, @des, @rep, @seg )";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd.Parameters.AddWithValue("@nom", temp.Nombre);
                cmd.Parameters.AddWithValue("@des", temp.Descripcion);
                cmd.Parameters.AddWithValue("@rep", temp.Repeticiones);
                cmd.Parameters.AddWithValue("@seg", temp.Segundos);

                Console.WriteLine(sql);

                cmd.ExecuteNonQuery();
            }
        }

        internal void Actualizar(EEjercicio temp)
        {
            string sql = "UPDATE app.ejercicios" +
                " SET nombre = @nom, descripcion = @des, repeticiones = @rep, segundos = @seg" +
                " WHERE id = @id";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd.Parameters.AddWithValue("@nom", temp.Nombre);
                cmd.Parameters.AddWithValue("@des", temp.Descripcion);
                cmd.Parameters.AddWithValue("@rep", temp.Repeticiones);
                cmd.Parameters.AddWithValue("@seg", temp.Segundos);
                cmd.Parameters.AddWithValue("@id", temp.Id);

                NpgsqlDataReader reader = cmd.ExecuteReader();
            }
        }

        internal EEjercicio CargarId(int id)
        {
            EEjercicio temp = new EEjercicio();
            string sql = "SELECT id, nombre, descripcion, repeticiones, segundos FROM app.ejercicios" +
                " WHERE id = " + id ;
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    temp = ArmarEjercicio(reader);
                }
            }
            return temp;
        }

        private EEjercicio ArmarEjercicio(NpgsqlDataReader reader)
        {
            EEjercicio temp = new EEjercicio
            {
                Id = reader.GetInt32(0),
                Nombre = reader.GetString(1),
                Descripcion = reader.GetString(2),
                Repeticiones = reader.GetInt32(3),
                Segundos = reader.GetInt32(4)
            };


            return temp;
        }

        internal List<EEjercicio> CargarTodosFiltro(string filtro)
        {
            List<EEjercicio> lista = new List<EEjercicio>();
            string sql = "SELECT id, nombre, descripcion, repeticiones, segundos FROM app.ejercicios" +
                " WHERE nombre like '" + filtro + "'";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lista.Add(ArmarEjercicio(reader));
                }
            }

            return lista;

        }
    }
}

