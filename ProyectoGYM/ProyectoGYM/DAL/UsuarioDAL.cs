﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using ProyectoGYM.Entities;

namespace ProyectoGYM.DAL
{
    class UsuarioDAL
    {
        /// <summary>
        /// Manda a pedir los datos a la base según la cedula y contraseña obtenidos en el login
        /// </summary>
        /// <param name="user"></param> EUsuario llenado con la cedula y la contraseña del login
        /// <returns></returns> el usuario completo obtenido de la base de datos
        internal EUsuario Inicio(EUsuario user)
        {
            string sql = String.Format("SELECT id, cedula, nombre, fecha_nac, email, tipo, contra, activo FROM app.usuarios " +
                "WHERE cedula = '{0}' AND contra = '{1}'", user.Cedula, user.Contrasena);
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {

                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return ArmarUsuario(reader);
                }
                return null;

            }
        }

        /// <summary>
        /// actualiza los datos de unusuario en la base de datos
        /// </summary>
        /// <param name="u"></param>EUsuario actualizado
        internal void ActualizarDatos(EUsuario u)
        {
            try
            {
                string sql = "UPDATE app.usuarios SET cedula = @ced, nombre = @nom, fecha_nac = @fec," +
                " email = @cor, tipo = @tip, contra = @con, activo = @act WHERE id = @id; ";
                

                using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
                {

                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@ced", u.Cedula);
                    cmd.Parameters.AddWithValue("@nom", u.Nombre);
                    cmd.Parameters.AddWithValue("@fec", u.FechaNac);
                    cmd.Parameters.AddWithValue("@cor", u.Correo);
                    cmd.Parameters.AddWithValue("@tip", u.Tipo);
                    cmd.Parameters.AddWithValue("@con", u.Contrasena);
                    cmd.Parameters.AddWithValue("@act", u.Activo);
                    cmd.Parameters.AddWithValue("@id", u.Id);
                    Console.WriteLine(sql);

                    cmd.ExecuteNonQuery();

                }
            }catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }   
        }

        internal List<EUsuario> CargarUsuarios1()
        {
            List<EUsuario> usuarios = new List<EUsuario>();
            string sql = "SELECT id, cedula, nombre, fecha_nac, email, tipo, contra, activo" +
                " FROM app.usuarios as u NATURAL JOIN app.datos as d WHERE u.tipo = 1 AND d.entrenador_id = u.id; ";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {

                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    usuarios.Add(ArmarUsuario(reader));
                }
                return usuarios;

            }
        }

        internal List<EUsuario> CargarUsuarios()
        {
            List<EUsuario> usuarios = new List<EUsuario>();
            string sql = "SELECT id, cedula, nombre, fecha_nac, email, tipo, contra, activo FROM app.usuarios " +
                "WHERE tipo = 3";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {

                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    usuarios.Add(ArmarUsuario(reader));
                }
                return usuarios;

            }
        }


        /// <summary>
        /// carga todos los usuarios sin filtro
        /// </summary>
        /// <returns></returns> una lista con todos los usuarios
        internal List<EUsuario> CargarTodos()
        {
            List<EUsuario> usuarios = new List<EUsuario>();
            string sql = "SELECT id, cedula, nombre, fecha_nac, email, tipo, contra, activo FROM app.usuarios";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {

                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    usuarios.Add( ArmarUsuario(reader));
                }
                return usuarios;

            }
        }

        /// <summary>
        /// Carga todos los usuarios con un filtro por cedula
        /// </summary>
        /// <param name="filtro"></param> cedula para filtrar datos
        /// <returns></returns> lista de usuarios según filtro
        internal List<EUsuario> CargarConFiltro(string filtro)
        {
            List<EUsuario> usuarios = new List<EUsuario>();
            string sql = "SELECT id, cedula, nombre, fecha_nac, email, tipo, contra, activo FROM app.usuarios " +
                "WHERE text(cedula) LIKE @fil";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {

                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@fil", "%" + filtro + "%");

                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {

                    usuarios.Add(ArmarUsuario(reader));
                }
                return usuarios;

            }
        }

        /// <summary>
        /// Registra los nuevos usuarios en la base de datos según en singup
        /// </summary>
        /// <param name="nuevo"></param> EUsuario con los datos del registro
        internal void Registrar(EUsuario nuevo)
        {
            string sql = "INSERT INTO app.usuarios( cedula, nombre, fecha_nac, email, contra)" +
                " VALUES( @ced, @nom, @fec, @ema, @con)";
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);

                cmd.Parameters.AddWithValue("@ced", nuevo.Cedula);
                cmd.Parameters.AddWithValue("@nom", nuevo.Nombre);
                cmd.Parameters.AddWithValue("@fec", nuevo.FechaNac);
                cmd.Parameters.AddWithValue("@ema", nuevo.Correo);
                cmd.Parameters.AddWithValue("@con", nuevo.Contrasena);

                NpgsqlDataReader reader = cmd.ExecuteReader();
            }
        }

        /// <summary>
        /// Arma un EUsuario con base a los resultados de los querys de la base de datos
        /// </summary>
        /// <param name="reader"></param> el reader que guarda los resultados de la base de datos
        /// <returns></returns> un EUsuario lleno con datos de los usuarios
        private EUsuario ArmarUsuario(NpgsqlDataReader reader)
        {
            EUsuario temp = new EUsuario
            {
                Id = reader.GetInt32(0),
                Cedula = reader.GetString(1),
                Nombre = reader.GetString(2),
                FechaNac = reader.GetDateTime(3),
                Correo = reader.GetString(4),
                Tipo = reader.GetInt32(5),
                Contrasena = reader.GetString(6),
                Activo = reader.GetBoolean(7)
            };
            return temp;
        }
    }
}
