--CREATE DATABASE proyecto_gym;

-- CREATE SCHEMA app;

CREATE TABLE app.usuarios
(
    id SERIAL NOT NULL PRIMARY KEY,
    cedula TEXT NOT NULL,
    nombre TEXT NOT NULL,
    fecha_nac TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
    email TEXT UNIQUE,
    tipo NUMERIC DEFAULT 3,
    contra TEXT  NOT NULL,
    activo BOOLEAN DEFAULT TRUE
);

CREATE TABLE app.ejercicios
(
	id SERIAL PRIMARY KEY,
	nombre TEXT, 
	descripcion TEXT NOT NULL,
	repeticiones NUMERIC NOT NULL,
	segundos NUMERIC NOT NULL
);

CREATE TABLE app.rutinas
(
	id SERIAL PRIMARY KEY,
	nombre TEXT NOT NULL,
	descripcion TEXT NOT NULL,
	ejercicios NUMERIC[] NOT NULL
);